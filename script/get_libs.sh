proj_home=`pwd`

# ##################################################################################
# Download NiTE2
# While finishing download unzip it, cd into the NiTE directory, run the install.sh
# Open the result file with the name NiTEDevEnvironment the change the variable
# name NITE2_REDIST64 to NITE2_REDIST
# Need to copy the Samples/Bin/NiTE2/ folder to the root of OpenISS
# ##################################################################################
echo 'download NiTE2 ...'
wget https://bitbucket.org/kaorun55/openni-2.2/raw/2f54272802bfd24ca32f03327fbabaf85ac4a5c4/NITE%202.2%20%CE%B1/NiTE-Linux-x64-2.2.tar.zip
echo 'finish downloading NiTE2'
echo '**********************************************'

# ##################################################################################
# Download OpenNI2
# If you are on Ubuntu18 just run the apt-get command to get the binary of OpenNI2
# the library will be installed under the path /usr/lib/ and the driver will be
# placed in /usr/lib/OpenNI2/Drivers/
# ##################################################################################

# git clone https://github.com/occipital/OpenNI2.git
# cd OpenNI2
# sudo apt-get install libusb-1.0-0-dev
# sudo apt-get install libudev-dev
# sudo apt-get install freeglut3-dev
# sudo apt-get install graphviz
# sudo apt-get install doxygen
# sudo apt-get install openjdk-8-jdk
# make

sudo apt-get install libopenni2-dev
echo '**********************************************'

# ##################################################################################
# Clone freenect2 and freenecct
# This two libraries is needed for running Kniect on OpenISS
# ##################################################################################

# build tools
sudo apt-get install build-essential cmake pkg-config

# freenect2
git clone https://github.com/OpenKinect/libfreenect2.git
cd libfreenect2
cd depends
./download_debs_trusty.sh
cd ..
sudo apt-get install libusb-1.0-0-dev
sudo apt-get install libturbojpeg0-dev
sudo apt-get install libglfw3-dev
sudo apt-get install beignet-dev
sudo apt-get install openni2-utils

mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/freenect2 -Dfreenect2_DIR=$HOME/freenect2/lib/cmake/freenect2
make
make install
sudo make install-openni2
sudo ln -s lib/OpenNI2-FreenectDriver/libfreenect2-openni2.so.0 \
           /usr/lib/OpenNI2/Drivers/libfreenect2-openni2.so.0

echo '**********************************************'

# freenect

# 1. build with openni2 support
# 2. goto build/lib/OpenNI2-FreenectDriver/
# 3. create a symbol link to libFreenectDriver.so.0.6.0 
#   from /usr/lib/OpenNI2/Drivers/ named with libFreenectDriver.so.0

# if you get error cannot open device, first run it with sudo
# then check the name of the library in /usr/lib/OpenNI2/Drivers for
# freenect, it has to be exactly the same as libFreenectDriver.so.0

git clone https://github.com/OpenKinect/libfreenect.git
cd libfreenect
mkdir build
cd build
cmake -L .. -DBUILD_OPENNI2_DRIVER=ON
make
sudo ln -s lib/OpenNI2-FreenectDriver/libFreenectDriver.so.0.6.0 \
           /usr/lib/OpenNI2/Drivers/libFreenectDriver.so.0

echo '**********************************************'


# ##################################################################################
# Build realsense
# This library is needed for running RealSense on OpenISS
# ##################################################################################

cd proj_home
git clone https://github.com/IntelRealSense/librealsense.git
cd librealsense

# Ubuntu 14 or when running of Ubuntu 16.04 live-disk:
# sudo apt-get install
# ./scripts/install_glfw3.sh 

# Ubuntu 16:
# sudo apt-get install libglfw3-dev

sudo apt-get install git libssl-dev libusb-1.0-0-dev pkg-config libgtk-3-dev
sudo apt-get install libglfw3-dev libgl1-mesa-dev libglu1-mesa-dev

sudo cp config/99-realsense-libusb.rules /etc/udev/rules.d/ 
sudo udevadm control --reload-rules && udevadm trigger
./scripts/patch-realsense-ubuntu-lts.sh

mkdir build && cd build
cmake ../ -DBUILD_EXAMPLES=true -DBUILD_GRAPHICAL_EXAMPLES=false
sudo make && sudo make install


echo '**********************************************'
# ##################################################################################
# Build OpenCV and its Contr Lib
# ##################################################################################

git clone https://github.com/opencv/opencv.git
git clone https://github.com/opencv/opencv_contrib.git

cd opencv
opencv_home=`pwd`
git checkout 3.4.1
mkdir build && cd build
opencv_bin=`pwd`
cd opencv_contrib
opencv_contr_home=`pwd`
git checkout 3.4.1
cd opencv_bin

sudo apt-get remove x264 libx264-dev
sudo apt-get install build-essential checkinstall cmake pkg-config yasm
sudo apt-get install git gfortran
sudo apt-get install libjpeg8-dev libjasper-dev libpng12-dev
sudo apt-get install libtiff5-dev

sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev
sudo apt-get install libxine2-dev libv4l-dev
sudo apt-get install libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev
sudo apt-get install qt5-default libgtk2.0-dev libtbb-dev
sudo apt-get install libatlas-base-dev
sudo apt-get install libfaac-dev libmp3lame-dev libtheora-dev
sudo apt-get install libvorbis-dev libxvidcore-dev
sudo apt-get install libopencore-amrnb-dev libopencore-amrwb-dev
sudo apt-get install x264 v4l-utils

# Optional dependencies
sudo apt-get install libprotobuf-dev protobuf-compiler
sudo apt-get install libgoogle-glog-dev libgflags-dev
sudo apt-get install libgphoto2-dev libeigen3-dev libhdf5-dev doxygen

sudo apt-get install python3-dev python3-pip
sudo -H pip3 install -U pip numpy

cmake -D CMAKE_BUILD_TYPE=RELEASE \
      -D CMAKE_INSTALL_PREFIX=/usr/local \
      -D INSTALL_C_EXAMPLES=ON \
      -D INSTALL_PYTHON_EXAMPLES=ON \
      -D WITH_TBB=ON \
      -D WITH_V4L=ON \
      -D WITH_QT=ON \
      -D WITH_OPENGL=ON \
      -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules \
      -D BUILD_TIFF=ON \
      -D BUILD_EXAMPLES=ON ..

make -j`nproc`
sudo make install
sudo sh -c 'echo "/usr/local/lib" >> /etc/ld.so.conf.d/opencv.conf'
sudo ldconfig

echo '**********************************************'

# install Anaconda
wget https://repo.anaconda.com/archive/Anaconda3-2018.12-Linux-x86_64.sh
bash Anaconda3-2018.12-Linux-x86_64.sh
conda create --name tf_gpu_env python=3.6 tensorflow-gpu
source activate tf_gpu_env
