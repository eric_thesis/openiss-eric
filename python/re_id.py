# -*- coding: utf-8 -*-

import random
import sys
import os
import h5py
import cv2
import numpy as np
import tensorflow.keras as keras
from keras.models import Model
from keras.layers import Dense, GlobalAveragePooling2D, Input
from m_resnet50 import ResNet50 as ResNet50WithStride1
from keras.applications.resnet50 import ResNet50, preprocess_input

# ------------------------------------------------------------------------
# get the path of the yolo model directory
# ------------------------------------------------------------------------
_path_prefix = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
_model_path = _path_prefix + '/reid_model/model_weights.h5'
# ------------------------------------------------------------------------



# ------------------------------------------------------------------------
# don't modify the following variables, unless you know what you are doing
# ------------------------------------------------------------------------
threshold = 9.0                         # obtain by experiment, maybe
                                        # different from model to model
num_classes = 743                       # from cuhk03
input_shape = (256, 128, 3)             # order: height, width, channel
last_stride = 1                         # stride last conv layer
database_path = '/reid_model/db.h5'      # database path
database_path =  _path_prefix + database_path
weights = 'imagenet'                    # useless here but keep it
# ------------------------------------------------------------------------



class ReIDer:

    '''
    This class encapsulate model obtained from deep conv neural network, expose
    the needed functionalities to OpenISS framework.
    '''

    def __init__(self, is_create=False):
        ''''''
        if last_stride == 1:
            self.base = ResNet50WithStride1(include_top=False, weights=weights,
                                            input_tensor=Input(shape=input_shape))
        elif last_stride == 2:
            self.base = ResNet50(include_top=False, weights=weights,
                                 input_tensor=Input(shape=input_shape))

        # according to num_classes make the dense layer
        feature = GlobalAveragePooling2D(name='GAP')(self.base.output)
        self.model = Model(inputs=self.base.input, outputs=feature)

        prediction = Dense(
            num_classes, activation='softmax', name='FC')(feature)
        _ = Model(inputs=self.base.input, outputs=prediction)

        self._model = Model(inputs=self.base.input, outputs=[prediction, feature])
        self._model.load_weights(_model_path)

        ''''''
        if not is_create:
            self.db = h5py.File(database_path)
        print('ReIDer model built successfully')

    def predict(self, query):
        '''
        Predict the query image is the same person with all identities in the database or not

        Arguments
            query: a single image in OpenCV format
        Return
            none: none of the person in the database (string "none")
            label: the id corresponding to the person (string format not int)
        '''
        # keras internally use PIL for image processing, convert
        # OpenCV format into that format first
        # https://github.com/qqwweee/keras-yolo3/issues/330
        rgb_format_query = cv2.cvtColor(query, cv2.COLOR_BGR2RGB)
        rgb_format_query = cv2.resize(rgb_format_query, (input_shape[1], input_shape[0]))
        image_array = np.array(rgb_format_query, dtype='float32')
        image_array = preprocess_input(image_array)
        image_array = np.expand_dims(image_array, 0)
        # invoke the model to do prediction
        query_feat = self.model.predict(image_array)
        dist, label = self._check_with_db(query_feat)

        if dist < threshold:
            return label
        else:
            return 'none'


    def _check_with_db(self, query_feat):
        '''
        Check the given feature map with the database

        Arguments
            query_feat: the query feature map produced by the model
        Return
            minimum distance, corresponding label with the min dist
        '''
        min_dist = np.finfo('float').max
        pred_label = None

        for key in self.db.keys():
            vals = self.db[key].value
            for feature in vals:
                dist = np.linalg.norm(feature - query_feat)
                if dist < min_dist:
                    min_dist = dist
                    pred_label = key

        return dist, str(pred_label)


def create_feature_database(model, input_image_path, db_path):
    from keras.preprocessing import image
    assert(os.path.isdir(input_image_path))
    if input_image_path[-1] == '/':
        input_image_path = input_image_path[:-1]
    ids = os.listdir(input_image_path)

    db = h5py.File(db_path, 'w')

    # loop over all the people
    for iid in ids:
        person_image_list = []
        cur_dir = input_image_path + '/' + iid
        assert(os.path.isdir(cur_dir))
        fns = os.listdir(cur_dir)

        # loop over all images for the same person
        for file_name in fns:
            path = cur_dir + '/' + file_name
            image_src = image.load_img(path, target_size=input_shape)
            img = image.img_to_array(image_src)
            img = preprocess_input(img)
            person_image_list.append(img)

        # feed to the model to calculate the features for that person
        x = np.array(person_image_list)
        person_feats = model.predict(x)

        # create db for each person
        db.create_dataset(iid, data=person_feats)
    print('---------------------------------------------')
    print('using model weight from: {}'.format(_model_path))
    print('finish creating database in: {}'.format(db_path))
    print('---------------------------------------------')


def crop_person(input_path, output_path):
    from yolo_model import YOLO
    detector = YOLO()

    if os.path.isdir(input_path):
        files = os.listdir(input_path)
    else:
        files = [input_path]

    for f in files:
        if f[-4:] == '.jpg':
            print('cropping person out from: ' + f)
            image = cv2.imread(input_path + '/' + f)
            bboxs = detector.detect_bbox(image).astype(int)
            counter = 0
            for box in bboxs:
                cropped = image[box[1]:box[3], box[0]:box[2]].copy()
                cv2.imwrite(output_path + '/' +
                            f[:-4] + '_' + str(counter) + '.jpg', cropped)
                counter += 1


if __name__ == "__main__":
    import sys
    args = sys.argv
    if len(args) != 4:
        print('wrong number of arguments.')
        exit(-1)

    option = args[1]
    if option == 'create':
        classifier = ReIDer(is_create=True)
        create_feature_database(classifier.model, args[2], args[3])
    elif option == 'crop':
        crop_person(args[2], args[3])
    elif option == 'test':
        # classifier = ReIDer()
        # img = cv2.imread(
        #     '/home/h_lai/Documents/testing/sample/reid/test/same/0583_c6s2_037093_01.jpg')
        # for i in range(10):
        #     res = classifier.predict(img)
        #     print(res)
        cap = cv2.VideoCapture(0)

        while(True):
            # Capture frame-by-frame
            ret, frame = cap.read()

            # Our operations on the frame come here
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            # Display the resulting frame
            cv2.imshow('frame', gray)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        # When everything done, release the capture
        cap.release()
        cv2.destroyAllWindows()
    else:
        print('not support command: {}'.format(option))
