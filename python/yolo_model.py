# -*- coding: utf-8 -*-
# reference https://github.com/qqwweee/keras-yolo3

from functools import reduce
from functools import wraps
import colorsys
import os
from timeit import default_timer as timer

import cv2 as cv
import numpy as np
import tensorflow as tf
from keras import backend as K
from keras.layers import Conv2D, Add, ZeroPadding2D, UpSampling2D, \
                         Concatenate, MaxPooling2D, Input
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.normalization import BatchNormalization
from keras.models import Model, load_model
from keras.regularizers import l2

# get the path of the yolo model directory
_path_prefix = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def compose(*funcs):
  '''
  Compose arbitrarily many functions, evaluated left to right.
  Reference: https://mathieularose.com/function-composition-in-python/
  '''
  if funcs:
    return reduce(lambda f, g: lambda *a, **kw: g(f(*a, **kw)), funcs)
  else:
    raise ValueError('Composition of empty sequence not supported.')

def letterbox_image(image, expected_size):
  '''
  This method make sure the image longest side respect
  the the max(expected_size). For example, the input
  image with the size [768, 576] and the expected size
  is [416, 416]. Then this method will keep the image
  ratio, and make it [416, 312]. Padding with the gray
  color (128, 128, 128).

  img: the image need to be resized
  expected_size: [height, width]
  '''
  ih, iw, _ = image.shape
  eh, ew = expected_size
  scale = min(eh / ih, ew / iw)
  nh = int(ih * scale)
  nw = int(iw * scale)

  image = cv.resize(image, (nw, nh), interpolation=cv.INTER_CUBIC)
  new_img = np.full((eh, ew, 3), 128, dtype='uint8')
  # fill new image with the resized image and centered it
  new_img[(eh - nh) // 2:(eh - nh) // 2 + nh,
          (ew - nw) // 2:(ew - nw) // 2 + nw,
          :] = image.copy()
  return new_img

@wraps(Conv2D)
def Darknet_Conv2D(*args, **kwargs):
  """Wrapper to set Darknet parameters for Convolution2D."""
  darknet_conv_kwargs = {'kernel_regularizer': l2(5e-4)}
  darknet_conv_kwargs['padding'] \
      = 'valid' if kwargs.get('strides') == (2, 2) else 'same'
  darknet_conv_kwargs.update(kwargs)
  return Conv2D(*args, **darknet_conv_kwargs)

def CBL(*args, **kwargs):
  """
  CBL = conv + bn + leaky relu
  CBL is the basic building block in Darknet53
  """
  no_bias_kwargs = {'use_bias': False}
  no_bias_kwargs.update(kwargs)
  return compose(
      Darknet_Conv2D(*args, **no_bias_kwargs),
      BatchNormalization(),
      LeakyReLU(alpha=0.1)
  )

def ResUnitN(x, num_filters, num_blocks):
  x = ZeroPadding2D(((1, 0),(1, 0)))(x)
  x = CBL(num_filters, (3, 3), strides=(2, 2))(x)  # downsampling
  for i in range(num_blocks):
      y = compose(
              CBL(num_filters // 2, (1, 1)),
              CBL(num_filters, (3, 3))
      )(x)
      x = Add()([x, y])  # main path + shortcut
  return x

def darknet_body(x):
  """
  According to YOLOv3's paper, define the Darknet53 backbone conv net.
  """
  x = CBL(32, (3,3))(x)
  x = ResUnitN(x, 64,   1)
  x = ResUnitN(x, 128,  2)
  x = ResUnitN(x, 256,  8)
  x = ResUnitN(x, 512,  8)
  x = ResUnitN(x, 1024, 4)
  return x

def last_layers(x, num_filters, out_filters):
  x = compose(
      CBL(num_filters,     (1, 1)),
      CBL(num_filters * 2, (3, 3)),
      CBL(num_filters,     (1, 1)),
      CBL(num_filters * 2, (3, 3)),
      CBL(num_filters,     (1, 1))
  )(x)

  y = compose(
      CBL(num_filters * 2, (3, 3)),
      Darknet_Conv2D(out_filters, (1, 1))
  )(x)
  return x, y

def yolo_backbone_net(inputs, num_anchors, num_classes):
  # 5 = 4 bounding box parameters + 1 objectness probability
  output_size = num_anchors * (num_classes + 5)

  darknet_model = Model(inputs, darknet_body(inputs))
  x, y1 = last_layers(darknet_model.output, 512, output_size)

  x = compose(
      CBL(256, (1 ,1)),
      UpSampling2D(2)
  )(x)
  x = Concatenate()([x, darknet_model.layers[152].output])
  x, y2 = last_layers(x, 256, output_size)

  x = compose(
      CBL(128, (1, 1)),
      UpSampling2D(2)
  )(x)
  x = Concatenate()([x, darknet_model.layers[92].output])
  x, y3 = last_layers(x, 128, output_size)

  print('info: Backbone net is constructed')
  return Model(inputs, [y1, y2, y3])

# WORK IN PROGRESS !!!!!

def draw_bbox(img, boxes):
  for b in boxes:
    cv.rectangle(img,
                 (b[0], b[1]), (b[2], b[3]),
                 (255, 0, 0), 1)
  return img


def correct_box(box_xy, box_wh, input_shape, image_shape):
  '''
  This method calculate the boxes and scores based on the
  raw input image, the value returned can be used directly
  to the user given image

  reference: https://zhuanlan.zhihu.com/p/49995236
  '''

  box_yx = box_xy[..., ::-1]
  box_hw = box_wh[..., ::-1]
  input_shape = K.cast(input_shape, K.dtype(box_yx))
  image_shape = K.cast(image_shape, K.dtype(box_yx))
  new_shape = K.round(image_shape * K.min(input_shape / image_shape))
  offset = (input_shape-new_shape)/2./input_shape
  scale = input_shape / new_shape
  box_yx = (box_yx - offset) * scale
  box_hw *= scale

  box_mins = box_yx - (box_hw / 2.)
  box_maxes = box_yx + (box_hw / 2.)
  boxes =  K.concatenate([
      box_mins[..., 0:1],  # y_min
      box_mins[..., 1:2],  # x_min
      box_maxes[..., 0:1],  # y_max
      box_maxes[..., 1:2]  # x_max
  ])

  # Scale boxes back to original image shape.
  boxes *= K.concatenate([image_shape, image_shape])
  return boxes


def box_and_score_(feat_i, anchors, num_classes,
                   input_img_shape, calc_loss=False):
  '''
  This method calculate the boxes and scores based on the
  pre-processed image resized to pre-defined size (416, 416)
  fed into the network.

  parameters
  ----------
  feat_i:               one of the output feature map y[i]
  anchors:             anchor priors for this scale, by default is 3
  num_classes:         number of classes of object can be detected
  input_img_shape:     the resized image shape (height and width) sent to
                       the backbone network, by default will be 416 * 416
  '''
  num_anchors = len(anchors)
  # reshape the tensor into:
  #   [batch, height, width, num_anchors, box_params]
  anchors_tensor = K.reshape(K.constant(anchors),
                             [1, 1, 1, num_anchors, 2])

  grid_shape = K.shape(feat_i)[1:3] # height, width
  # create index for each grid cell
  cell_idx_y = K.arange(0, stop=grid_shape[0])
  cell_idx_x = K.arange(0, stop=grid_shape[1])
  grid_y = K.tile(K.reshape(cell_idx_y, [-1, 1, 1, 1]),
                  # expand col into row's direction
                  [1, grid_shape[1], 1, 1])
  grid_x = K.tile(K.reshape(cell_idx_x, [1, -1, 1, 1]),
                  # expand row into column's direction
                  [grid_shape[0], 1, 1, 1])
  # concat into grid
  grid = K.cast(K.concatenate([grid_x, grid_y]), K.dtype(feat_i))

  feat_i = K.reshape(
      feat_i,
      [-1, grid_shape[0], grid_shape[1], num_anchors, num_classes + 5]
  )

  # --- why here we need to divide by the feature map's size? ---
  # since we want to limit all the data into [0, 1]
  # the pobabilities of objetness score are in [0, 1]
  # if the location is not within the region, the model cannot
  # have a good convergence
  #
  # box_xy location is relative to the feature map's size
  # box_hw is relative to the resized image fed to the network
  #
  # reference: https://zhuanlan.zhihu.com/p/49995236
  # ---
  box_xy = ((K.sigmoid(feat_i[..., :2]) + grid)
            / K.cast(grid_shape[::-1], K.dtype(feat_i)))
  box_wh = (K.exp(feat_i[..., 2:4]) * anchors_tensor
            / K.cast(input_img_shape[::-1], K.dtype(feat_i)))
  box_conf = K.sigmoid(feat_i[..., 4:5])
  box_class_probs = K.sigmoid(feat_i[..., 5:])

  if calc_loss:
    return grid, feat_i, box_xy, box_wh

  return box_xy, box_wh, box_conf, box_class_probs


def box_and_score(feat_i, anchors, num_classes,
                  input_img_shape, raw_img_shape):
  # get result based on resized image
  b_xy, b_wh, b_conf, b_probs = box_and_score_(
      feat_i, anchors, num_classes, input_img_shape
  )
  # get result based on raw input image
  boxes = correct_box(b_xy, b_wh, input_img_shape, raw_img_shape)
  boxes = K.reshape(boxes, [-1, 4])
  boxes_scores = K.reshape(b_conf * b_probs, [-1, num_classes])
  return boxes, boxes_scores


def yolo_eval(feats, anchors, num_classes, raw_img_shape,
            max_boxes=20, obj_score_threshold=0.6, iou_threshold=0.5):
  '''
  parameters
  ----------
  feats:               output feature map from the network [y1, y2, y3]
  anchors:             anchor priors list
  num_classes:         number of classes of object can be detected
  raw_img_shape:       the original image size, need it for mapping
                       the bbox back
  max_boxes:           maximum number of returning boxes from NMS
  obj_score_threshold: objectness score lower than it will be discarded
  iou_threshold:

  return
  ------
  boxes_:   the predctive box in the input image
  scores_:  the predictive score for current class
  classes_: the predictive class index in the pre-defined list
  '''
  # by default it will be 3
  num_layers = len(feats)
  # each output yi takes 3 anchors
  anchor_mask = [[6, 7, 8], [3, 4, 5], [0, 1, 2]]

  # ----
  # why? [1:3] means height and width
  input_img_shape = K.shape(feats[0])[1:3] * 32
  # ----

  boxes = []
  scores = []

  for l in range(num_layers):
    # loop over all output yi, do the prediction
    _boxes, _scores = \
      box_and_score(feats[l], anchors[anchor_mask[l]], num_classes,
                    input_img_shape, raw_img_shape)
    boxes.append(_boxes)
    scores.append(_scores)

  # collect all boxes and scores in to column verctor
  boxes = K.concatenate(boxes, axis=0)
  scores = K.concatenate(scores, axis=0)
  mask = scores >= obj_score_threshold

  # apply non-max suppression
  boxes_ = []
  scores_ = []
  classes_ = []

  for c in range(num_classes):
    class_boxes = tf.boolean_mask(boxes, mask[:, c])
    class_boxes_score = tf.boolean_mask(scores[:, c], mask[:, c])
    nms_index = tf.image.non_max_suppression(
        class_boxes, class_boxes_score,
        K.constant(max_boxes, dtype='int32'),
        iou_threshold=iou_threshold
    )
    class_boxes = K.gather(class_boxes, nms_index)
    class_boxes_score = K.gather(class_boxes_score, nms_index)
    classes = K.ones_like(class_boxes_score, 'int32') * c
    boxes_.append(class_boxes)
    scores_.append(class_boxes_score)
    classes_.append(classes)

  boxes_ = K.concatenate(boxes_, axis=0)
  scores_ = K.concatenate(scores_, axis=0)
  classes_ = K.concatenate(classes_, axis=0)

  return boxes_, scores_, classes_

# export the YOLO functionality to the user
# encapsulate the functions defined above into
# a Keras + tensorflow model

class YOLO(object):
  # class variable, shared by all instances
  _defaults = {
    "model_path": _path_prefix + '/yolo_model/yolo.h5',
    "anchors_path": _path_prefix + '/yolo_model/yolo_anchors.txt',
    "classes_path": _path_prefix + '/yolo_model/voc_person.txt',
    "score" : 0.3,
    "iou" : 0.45,
    "model_image_size" : (416, 416),
    "gpu_num" : 1,
  }

  # return the required key's value if key is
  # in _default dict defined above
  @classmethod
  def get_defaults(cls, key):
    if key in cls._defualts:
      return cls._defaults[key]
    else:
      return "unrecognized attribute name '" + key + "'"

  def __init__(self, **kwargs):
    self.__dict__.update(self._defaults) # set up default values
    self.__dict__.update(kwargs) # and update with user overrides
    self.class_names = self._get_class()
    self.anchors = self._get_anchors()
    self.sess = K.get_session()
    self.input_image_shape = K.placeholder(shape=(2, ))
    self.init()
    self.boxes, self.scores, self.classes = self.eval()

  def _get_class(self):
    classes_path = os.path.expanduser(self.classes_path)
    with open(classes_path) as f:
      class_names = f.readlines()
      class_names = [c.strip() for c in class_names]
#       print('info: class names: ' + str(class_names))
      return class_names

  def _get_anchors(self):
    anchors_path = os.path.expanduser(self.anchors_path)
    with open(anchors_path) as f:
      anchors = f.readline()
      anchors = [float(x) for x in anchors.split(',')]
      anchors_ = np.array(anchors).reshape(-1, 2)
      return anchors_

  def init(self):
    model_path = os.path.expanduser(self.model_path)
    assert (model_path.endswith('.h5'),
            'Keras model or weights must be a .h5 file.')

    num_anchors = len(self.anchors)
    num_classes = len(self.class_names)

    # todo: add tiny model later
    try:
      self.yolo_model = load_model(model_path, compile=False)
    except:
      self.yolo_model = yolo_backbone_net(
          Input(shape=(None, None, 3)), num_anchors // 3, num_classes
      )
      self.yolo_model.load_weights(self.model_path)
      print('cannot load model from file')
    else:
      assert self.yolo_model.layers[-1].output_shape[-1] == \
        num_anchors / len(self.yolo_model.output) * (num_classes + 5), \
        'Mismatch between model and given anchors and classes size'
    print('{} model, anchors, and classes loaded.'.format(model_path))

    # Generate colors for drawing bounding boxes.
    hsv_tuples = [(x / len(self.class_names), 1., 1.)
                  for x in range(len(self.class_names))]
    self.colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
    self.colors = list(
        map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)),
            self.colors)
    )
    np.random.seed(10101)
    np.random.shuffle(self.colors)
    np.random.seed(None)

  def eval(self):
    boxes, scores, classes = yolo_eval(
        self.yolo_model.output,
        self.anchors, len(self.class_names), self.input_image_shape,
        obj_score_threshold=self.score, iou_threshold=self.iou
    )
    return boxes, scores, classes

  def detect_bbox(self, image):
    out_boxes, out_scores, out_classes = self.detect_bbox_(image)
    res = []
    for i, c in reversed(list(enumerate(out_classes))):
      box = out_boxes[i]
      score = out_scores[i]

      top, left, bottom, right = box
      top = max(0, np.floor(top + 0.5).astype('int32'))
      left = max(0, np.floor(left + 0.5).astype('int32'))
      bottom = min(image.shape[0], np.floor(bottom + 0.5).astype('int32'))
      right = min(image.shape[1], np.floor(right + 0.5).astype('int32'))

      tmp = [left, top, right, bottom, score]
      res.append(tmp)

    boxes_np = np.array(res, dtype=np.float32)
    return boxes_np

  def detect_bbox_(self, image):
    start_timer = timer()

    assert self.model_image_size[0]%32 == 0, 'Multiples of 32 required'
    assert self.model_image_size[1]%32 == 0, 'Multiples of 32 required'
    # letterbox is done by OpenCV, we need to convert into PIL format
    boxed_image = letterbox_image(image, tuple(self.model_image_size))
    boxed_image_ = cv.cvtColor(boxed_image, cv.COLOR_BGR2RGB)

    image_data = np.array(boxed_image_, dtype='float32')
    # print(image_data.shape)

    image_data /= 255.
    image_data = np.expand_dims(image_data, 0)

    out_boxes, out_scores, out_classes = self.sess.run(
      # run the eval() method
      [self.boxes, self.scores, self.classes],
      feed_dict={
        self.yolo_model.input: image_data,
        self.input_image_shape: [image.shape[0], image.shape[1]],
        K.learning_phase(): 0
      }
    )
    # print('total time for detection: ' + str(timer() - start_timer))
    return out_boxes, out_scores, out_classes

  def detect_image(self, image):
    out_boxes, out_scores, out_classes = self.detect_bbox_(image)

    font = cv.FONT_HERSHEY_PLAIN
    font_scale = 1.0
    thickness = (image.shape[1] + image.shape[0]) // 300

    for i, c in reversed(list(enumerate(out_classes))):
      predicted_class = self.class_names[c]
      box = out_boxes[i]
      score = out_scores[i]

      label = '{} {:.2f}'.format(predicted_class, score)

      top, left, bottom, right = box
      top = max(0, np.floor(top + 0.5).astype('int32'))
      left = max(0, np.floor(left + 0.5).astype('int32'))
      bottom = min(image.shape[0], np.floor(bottom + 0.5).astype('int32'))
      right = min(image.shape[1], np.floor(right + 0.5).astype('int32'))
      print(label, (left, top), (right, bottom))

      # draw the bounding box into the image
      cv.rectangle(image, (left, top), (right, bottom), self.colors[c], thickness)

      label_size, _ = cv.getTextSize(label, font, font_scale, thickness)
      if top - label_size[0] >= 0:
        text_origin = np.array([left, top - label_size[0]])
      else:
        text_origin = np.array([left, top - 1])

      cv.putText(image, label, tuple(text_origin), font, font_scale, self.colors[c], 2)
    return image

  def close_session(self):
    self.sess.close()
