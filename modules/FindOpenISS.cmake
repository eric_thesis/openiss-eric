###############################################################################
# Find OpenISS
#
#     find_package(OpenISS)
#
# Variables defined by this module:
#
#  OPENISS_FOUND               True if OpenISS was found
#  OPENISS_INCLUDE_DIRS        The location(s) of OpenISS headers
#  OPENISS_LIBRARIES           Libraries needed to use OpenISS


FIND_PATH(
    OPENISS_INCLUDE_DIR OpenISS.h
    "${PROJECT_BINARY_DIR}/include"
    /usr/local/include/openiss /usr/include/openiss $ENV{OPENISS_INCLUDE} ${OPENISS_INCLUDE}
)
FIND_LIBRARY(
    OPENISS_LIBRARY openiss
    "${PROJECT_BINARY_DIR}/lib"
    /usr/local/lib/openiss /usr/lib/openiss $ENV{OPENISS_LIBRARY}  ${OPENISS_LIBRARY}
)

set(OPENISS_INCLUDE_DIRS ${OPENISS_INCLUDE_DIR})
set(OPENISS_LIBRARIES ${OPENISS_LIBRARY})
set(OPENISS_LIBS_DIRS "${PROJECT_BINARY_DIR}/lib")

IF (OPENISS_INCLUDE_DIR AND OPENISS_LIBRARY)
    # message("OPENISS_INCLUDE_DIRS and OPENISS_LIBRARY found")
    # message("openiss lib -> " ${OPENISS_LIBRARY})
    set(OPENISS_FOUND TRUE)
    include_directories(${OPENISS_INCLUDE_DIRS})

ELSE(OPENISS_INCLUDE_DIR AND OPENISS_LIBRARY)
    message("OPENISS_INCLUDE_DIR AND OPENISS_LIBRARY not found")
ENDIF (OPENISS_INCLUDE_DIR AND OPENISS_LIBRARY)
#message("= = = = = = = = = = = = = = = = = = = = = = = = ")