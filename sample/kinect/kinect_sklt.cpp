//
// Created by Haotao Lai on 2018-09-18.
//

#include <opencv/cv.hpp>
#include "../../src/include/OpenISS.h"

using namespace openiss;

void displaySkeleton();

int main() {
    displaySkeleton();
    return 0;
}

void displaySkeleton() {
    const char* win1Name = "color image";
    cv::namedWindow(win1Name);

    // create an kinect device, enable all features
    OIDevFactory devFactory;
    shared_ptr<OIDevice> pDevice = devFactory.create("kinect");
    pDevice->open();
    pDevice->enable();

    // create tracker
    OITrackerFactory trackerFactory;
    shared_ptr<OITracker> tracker = trackerFactory.createTracker("nite", pDevice.get());
    shared_ptr<OITrackerFrame> trackerFrame = trackerFactory.createTrackerFrame("nite");

    bool shutdown = false;
    while (!shutdown) {
        // get the registered color frame
        OIFrame* displayFrame = pDevice->readFrame(COLOR_STREAM);
        tracker->readFrame(trackerFrame.get());

        // obtain skeleton and draw it into the color frame
        std::vector<std::shared_ptr<OIUserData>> users = trackerFrame->getUsers();
        for (const auto &u : users) {
            OISkeleton *pSkeleton = u->getSkeleton();
//            pSkeleton->mapWorld2Image(tracker.get());
            pSkeleton->drawToFrame(displayFrame, trackerFrame->getSupportedJointType());
        }

        // display the frame
        displayFrame->show(win1Name);
        int key = cv::waitKey(1);
        shutdown = shutdown || (key > 0 && ((key & 0xFF) == 27)); // shutdown on esc
    }
}
