add_executable(kinect_img kinect_capture.cpp)
add_executable(kinect_skeleton kinect_sklt.cpp)

target_link_libraries(kinect_img openiss)
target_link_libraries(kinect_skeleton openiss)

include(utilities)
set(is_debug 0)

link_sample_basic_libs("kinect_img")
link_sample_basic_libs("kinect_skeleton")