//
// Created by Haotao Lai on 2018-08-08.
//

#include <opencv/cv.hpp>
#include <cstring>
#include "../../src/include/OpenISS.h"

using namespace openiss;

int main() {
    const std::string sample_root_path = PROJECT_SAMPLE_ROOT;
    const char* win1Name = "color image";
    const char* win2Name = "depth image";

    cv::namedWindow(win1Name);
    cv::namedWindow(win2Name);

    OIDevFactory factory;
    std::shared_ptr<OIDevice> pDevice = factory.create("kinect");
    pDevice->open();
    pDevice->enableColor();
    pDevice->enableDepth();

    int cc = 1;
    int dc = 1;
    bool shutdown = false;
    while (!shutdown) {
        OIFrame *colorFrame = pDevice->readFrame(openiss::COLOR_STREAM);
        OIFrame *depthFrame = pDevice->readFrame(openiss::DEPTH_STREAM);
        colorFrame->show(win1Name);
        depthFrame->show(win2Name);

        int key = cv::waitKey(1);
        shutdown = shutdown || (key > 0 && ((key & 0xFF) == 27));

        // save image
        switch (key & 0xFF) {
            case 's':
                colorFrame->save(sample_root_path + "kinect/saved_images/",
                                 "color" + std::to_string(cc++));
                break;
            default:
                break;
        }
    }

    return 0;
}

