//
// Created by Haotao Lai on 2018-10-24.
//

#include <iostream>
#include "OIDataFrame.h"
#include "OIFrameCVImpl.h"

void openiss::OIDataFrame::createDisplayImg() {
    if (type == FrameType::DEPTH_FRAME) {
        cv::Mat img(height, width, CV_16U, mpData);
        displayCVImpl = new OIFrameCVImpl(img);
    }
    else if (type == FrameType::COLOR_FRAME) {
        cv::Mat img(height, width, CV_8UC3, mpData);
        displayCVImpl = new OIFrameCVImpl(img);
    }
    else if (type == FrameType::IR_FRAME) {
        cv::Mat img(height, width, CV_8UC1, mpData);
        displayCVImpl = new OIFrameCVImpl(img);
    }
    else {
        assert("not support frame type");
    }
}

openiss::OIDataFrame::OIDataFrame(FrameType type, void *data, int bpp, int width, int height)
  : type(type), mpData(data), bpp(bpp), width(width), height(height)
  , displayCVImpl(nullptr), hasDispImg(false), hasOwnData(false)
{ }

openiss::OIDataFrame::OIDataFrame(openiss::OIDataFrame &frame)
  : type(frame.type), bpp(frame.getBytesPerPixel()), width(frame.getWidth()), height(frame.getHeight())
  , displayCVImpl(nullptr), hasDispImg(false), hasOwnData(true)
{
    int len = width * height * bpp;
    mpData = new uint8_t[len];
    memcpy(mpData, frame.getData(), static_cast<size_t>(len));
}

void * openiss::OIDataFrame::getData() const {
    return mpData;
}

openiss::OIFrame* openiss::OIDataFrame::getOIFrame() {
    if (!hasDispImg) { createDisplayImg(); }
    return displayCVImpl;
}

int openiss::OIDataFrame::getBytesPerPixel() const {
    return bpp;
}

int openiss::OIDataFrame::getHeight() const {
    return height;
}

int openiss::OIDataFrame::getWidth() const {
    return width;
}

void openiss::OIDataFrame::save(std::string path, std::string fileName) {
    if (!hasDispImg) { createDisplayImg(); }
    displayCVImpl->save(path, fileName);
}

void openiss::OIDataFrame::show(const char *winName) {
    if (!hasDispImg) { createDisplayImg(); }
    displayCVImpl->show(winName);
}

void openiss::OIDataFrame::drawSkeleton(openiss::OISkeleton *pSkeleton, vector<JointType> &types) {
    if (!hasDispImg) { createDisplayImg(); }
    displayCVImpl->drawSkeleton(pSkeleton, types);
}

openiss::OIDataFrame::~OIDataFrame() {
    if (hasOwnData) {
        if (type == FrameType::DEPTH_FRAME) {
            delete[] (uint16_t *) mpData;
        }
        else if (type == FrameType::COLOR_FRAME) {
            delete[] (uint8_t *) mpData;
        }
        else if (type == FrameType::IR_FRAME) {
            delete[] (uint8_t *) mpData;
        }
        else {
            assert("cannot delete memory with void type");
        }
    }
    delete displayCVImpl;
}

