//
// Created by h_lai on 13/02/19.
//

#ifndef OPENISS_OIYOLODETECTOR_H
#define OPENISS_OIYOLODETECTOR_H

#include "OIPythonEnv.h"
#include "include/OIType.h"
#include <opencv/cv.hpp>
#include <numpy/ndarrayobject.h>
#include <vector>
#include <iostream>

using namespace std;
using namespace cv;

namespace openiss {
class OIYoloDetector {
public:
    OIYoloDetector();
    ~OIYoloDetector();

    Mat detect_image(Mat &input);
    void detect_bbox(Mat &input, vector<vector<openiss::Point2i>> &bbox, vector<float> &scores);
private:
    PyObject *pInstance, *pResult, *pImageArg;
    PyObject *pFuncDetectBbox, *pFuncDetectImage;
    OIPythonEnv env;

    int init();
};
} // end of namespace

#endif //OPENISS_OIYOLODETECTOR_H
