//
// Created by h_lai on 28/03/19.
//

#ifndef OPENISS_ReIDer
#define OPENISS_ReIDer

#include "OIPythonEnv.h"
#include "include/OIType.h"
#include <opencv/cv.hpp>
#include <numpy/ndarrayobject.h>
#include <vector>
#include <cstring>
#include <iostream>

using namespace std;
using namespace cv;

namespace openiss {
class ReIDer {
public:
    ReIDer();
    ~ReIDer();

    string predict(Mat &image1);

private:
    PyObject *pInstance, *pResult, *pImageArg1;
    PyObject *pFuncPredict;
    OIPythonEnv env;

    int init();
};
}

#endif //OPENISS_ReIDer
