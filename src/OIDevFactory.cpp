//
// Created by Haotao Lai on 2018-08-09.
//

#include <cstring>
#include "include/OIDevice.h"
#include "OIKinect.h"
#include "OIRealSenseD435.h"

std::shared_ptr<openiss::OIDevice> openiss::OIDevFactory::create(const char *devName) {
    if (std::strcmp(devName, "kinect") == 0) {

        if (!mIsOpenNIInit) {
            openni::OpenNI::initialize();
            mIsOpenNIInit = true;
        }
        std::shared_ptr<OIDevice> pDev(new OIKinect);
        return pDev;
    }
    else if (std::strcmp(devName, "rs_d435") == 0) {
        std::shared_ptr<OIDevice> pDev(new OIRealSenseD435);
        return pDev;
    }
    else {
        std::cout << "cannot create a device" << std::endl;
        exit(1);
    }
}
