//
// Created by Haotao Lai on 2018-08-09.
//

#ifndef OPENISS_OIFRAMEIMPL_H
#define OPENISS_OIFRAMEIMPL_H

#include "include/OIFrame.h"
#include "include/OISkeleton.h"
#include <opencv2/core/mat.hpp>
#include <opencv/cv.hpp>

namespace openiss {

class OIFrameCVImpl : public OIFrame {

public:
    explicit OIFrameCVImpl(const cv::Mat& img);

    int getHeight() const override;
    int getWidth()  const override;

    void save(std::string path, std::string fileName);
    void show(const char* winName);
    void drawSkeleton(OISkeleton *pSkeleton, vector<JointType> &types);

    cv::Mat getCvMat() const;

private:
    cv::Mat mImg;
};

} // end of namespace
#endif //ERIC_THESIS_TESTING_OIFRAMEIMPL_H
